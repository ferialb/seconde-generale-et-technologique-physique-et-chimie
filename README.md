# SECONDE GENERALE ET TECHNOLOGIQUE - PHYSIQUE ET CHIMIE

**Bienvenue sur votre tableau de bord ** Bon voyage 

# LA RENTREE 2021

## I-ONDES ET SIGNAUX
### Vision et image
#### 1-La propagation de la lumière
#### 2-Les spectres lumineux
#### 3-Les lentilles, le modèle de la lentille mince convergente

## CONSTITUTION ET TRANSFORMATIONS DE LA MATIERE
### Constitution de la matière de l'échelle macroscopique à l'échelle microscopique
#### Description et caractérisation de la matière à l'échelle macroscopique
##### Corps purs et mélanges au quotidien
